/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ufpr.sistemapedidos;

import java.util.List;

public class Pedido {

    private Integer id;
    private String data;
    private Cliente cliente;
    private List<ItemDoPedido> itens;

    public Pedido(int id, String data, Cliente cliente, List<ItemDoPedido> itens) {
        this.id = id;
        this.data = data;
        this.cliente = cliente;
        this.itens = itens;
    }

    public Pedido() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Cliente getCliente() {

        return this.cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<ItemDoPedido> getItens() {
        return itens;
    }

    public void setItens(List<ItemDoPedido> itens) {
        this.itens = itens;
    }

    @Override
    public String toString() {
        String _id = (this.id != null) ? this.id + "" : "_";
        String _data = (this.data != null) ? this.data : "____/__/__";
        String _cliente = (this.cliente != null) ? this.cliente.toString() : "SSSSSSSSSSSS SSSSSSSSSS";
        String _itens = (this.itens != null) ? this.itens.toString() : "IIIIII";
        return "#" + _id + " | " + _data + " | " + _cliente + " | " + _itens;
    }
}
