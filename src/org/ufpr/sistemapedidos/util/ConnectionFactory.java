/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ufpr.sistemapedidos.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 *
 * @author noemi
 */
public class ConnectionFactory {

    private String dbDriver;
    private String dbUser;
    private String dbPasswd;
    private String dbHost;
    private String dbName;
    private String dbAutoReconnect;
    private String dbMaxReconnects;

    private Properties connProperties;
    private String dbURL;

    public ConnectionFactory() throws IOException {
        this.readProperties();
        this.setProperties();
        this.setDBURL();
    }

    public Connection getConnection() {
        try {
            return DriverManager.getConnection(dbURL, connProperties);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    private void setProperties() {
        this.connProperties = new Properties();
        this.connProperties.put("user", this.dbUser);
        this.connProperties.put("password", this.dbPasswd);
        this.connProperties.put("autoReconnect", this.dbAutoReconnect);
        this.connProperties.put("maxReconnects", this.dbMaxReconnects);
    }

    private void setDBURL() {
        this.dbURL = this.dbDriver + "://" + this.dbHost + "/" + this.dbName;
    }

    private void readProperties() throws IOException {
        String path = System.getProperty("user.dir") + File.separator + "conf" + File.separator + "db.properties";
        InputStream input = new FileInputStream(path);
        Properties prop = new Properties();
        prop.load(input);
        
        this.dbDriver = prop.getProperty("db.driver");
        this.dbHost = prop.getProperty("db.host");
        this.dbName = prop.getProperty("db.name");
        this.dbUser = prop.getProperty("db.user");
        this.dbPasswd = prop.getProperty("db.password");
        this.dbAutoReconnect = prop.getProperty("db.auto_reconnect");
        this.dbMaxReconnects = prop.getProperty("db.max_reconnects");
    }
}
