/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ufpr.sistemapedidos.swing.table;


import java.util.ArrayList;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import org.ufpr.sistemapedidos.Pedido;

/**
 *
 * @author Noemi
 */
public class ModeloTabelaPedido extends AbstractTableModel{
    private String[] colunas=new String[]{"id","data","CPF Cliente", "Nome do Cliente"};

    private List<Pedido> lista=new ArrayList<>();

    
    public ModeloTabelaPedido(List<Pedido> lista){
        this.lista=lista;
    }

    public ModeloTabelaPedido(){
    }


    @Override
    public int getRowCount() {
        return this.lista.size();
    }

    @Override
    public int getColumnCount() {
        return this.colunas.length;
    }

    @Override
    public String getColumnName(int index) {
        return this.colunas[index];
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
        //if(column==0)
            //return false;
        //return true;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Pedido pedido = lista.get(rowIndex);
        switch (columnIndex) {
            case 0: return pedido.getId();//if column 0 (code)
            case 1: return pedido.getData();//if column 1 (data)
            case 2: return pedido.getCliente().getCpf();//if column 2 (fk cliente)
            case 3: return pedido.getCliente().getNome();//if column 2 (fk cliente)
            default : return null;
        }
    }

    @Override
    public void setValueAt(Object value, int row, int col) {
        Pedido Pedido = lista.get(row);
        switch (col) {
            case 0:
                Pedido.setId((Integer) value);
                break;
            case 1:
                Pedido.setData((String) value);
                break;
            case 2:
                Pedido.getCliente().setCpf((String) value); 
                break;     
            case 3:
                Pedido.getCliente().setNome((String) value); 
                break;     
            default:
        }
        this.fireTableCellUpdated(row, col);
    }

    public boolean removePedido(Pedido pedido) {
        int linha = this.lista.indexOf(pedido);
        boolean result = this.lista.remove(pedido);
        this.fireTableRowsDeleted(linha,linha);//update JTable
        return result;
    }

    public void adicionaPedido(Pedido pedido) {
        this.lista.add(pedido);
        //this.fireTableDataChanged();
        this.fireTableRowsInserted(lista.size()-1,lista.size()-1);//update JTable
    }

    public void setListaPedidos(List<Pedido> pedidos) {
        this.lista = pedidos;
        this.fireTableDataChanged();
        this.fireTableRowsInserted(0,pedidos.size()-1);//update JTable
    }

    public void limpaTabela() {
        int indice = lista.size()-1;
        if(indice<0)
            indice=0;
        this.lista = new ArrayList<>();
        this.fireTableRowsDeleted(0,indice);//update JTable
    }

    public Pedido getPedido(int linha){
        return lista.get(linha);
    }
    
}
