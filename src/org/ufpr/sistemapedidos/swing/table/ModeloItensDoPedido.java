/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ufpr.sistemapedidos.swing.table;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.AbstractTableModel;
import org.ufpr.sistemapedidos.ItemDoPedido;
import org.ufpr.sistemapedidos.Pedido;
import org.ufpr.sistemapedidos.Produto;
import org.ufpr.sistemapedidos.builders.ProdutoBuilder;
import org.ufpr.sistemapedidos.dao.ItemDoPedidoDAO;
import org.ufpr.sistemapedidos.dao.impl.ItemDoPedidoDAOImpl;

/**
 *
 * @author luan
 */
public class ModeloItensDoPedido extends AbstractTableModel {

    private final String[] colunas = new String[]{"Descrição", "Qtd"};

    private List<ItemDoPedido> lista = new ArrayList<>();

    public ModeloItensDoPedido(List<ItemDoPedido> lista) {
        this.lista = lista;
    }

    public ModeloItensDoPedido() {
    }

    @Override
    public int getRowCount() {
        return this.lista.size();
    }

    @Override
    public int getColumnCount() {
        return this.colunas.length;
    }

    @Override
    public String getColumnName(int index) {
        return this.colunas[index];
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
        //if(column==0)
        //return false;
        //return true;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        ItemDoPedido itemDoPedido = lista.get(rowIndex);
        switch (columnIndex) {
            case 0: //if column 1 (name)
                return itemDoPedido.getProduto().getDescricao();
            case 1: //if column 2 (name)
                return itemDoPedido.getQuantidade();
            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object value, int row, int col) {
        ItemDoPedido itemDoPedido = lista.get(row);
        switch (col) {
            case 0:  //if column 0 (code)
                Produto produto = new ProdutoBuilder().setDescricao((String) value).build();
                itemDoPedido.setProduto(produto);
                break;
            case 1: //if column 1 (code)
                itemDoPedido.setQuantidade((Integer) value);
                break;
            default:
        }
        this.fireTableCellUpdated(row, col);
    }

    public boolean removeItemDoPedido(ItemDoPedido product) {
        int linha = this.lista.indexOf(product);
        boolean result = this.lista.remove(product);
        this.fireTableRowsDeleted(linha, linha);//update JTable
        return result;
    }

    public void adicionaItemDoPedidoo(ItemDoPedido product) {
        this.lista.add(product);
        //this.fireTableDataChanged();
        this.fireTableRowsInserted(lista.size() - 1, lista.size() - 1);//update JTable
    }

    public void setListaItensDoPedido(List<ItemDoPedido> products) {
        this.lista = products;
        this.fireTableDataChanged();
        //this.fireTableRowsInserted(0,contatos.size()-1);//update JTable
    }

    public void limpaTabela() {
        int indice = lista.size() - 1;
        if (indice < 0) {
            indice = 0;
        }
        this.lista = new ArrayList<>();
        this.fireTableRowsDeleted(0, indice);//update JTable
    }

    public ItemDoPedido getItemDoPedido(int linha) {
        return lista.get(linha);
    }

    public List<ItemDoPedido> getItensDoPedido() {
        return this.lista;
    }
}
