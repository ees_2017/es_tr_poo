/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ufpr.sistemapedidos.swing.table;

import java.util.ArrayList;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import org.ufpr.sistemapedidos.Produto;

/**
 *
 * @author luan
 */
public class ModeloTabelaProduto extends AbstractTableModel{
    private String[] colunas=new String[]{"id","descricao"};

    private List<Produto> lista=new ArrayList<>();

    
    public ModeloTabelaProduto(List<Produto> lista){
        this.lista=lista;
    }

    public ModeloTabelaProduto(){
    }


    @Override
    public int getRowCount() {
        return this.lista.size();
    }

    @Override
    public int getColumnCount() {
        return this.colunas.length;
    }

    @Override
    public String getColumnName(int index) {
        return this.colunas[index];
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
        //if(column==0)
            //return false;
        //return true;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Produto product = lista.get(rowIndex);
        switch (columnIndex) {
            case 0: return product.getId();//if column 0 (code)
            case 1: return product.getDescricao();//if column 1 (name)
            default : return null;
        }
    }

    @Override
    public void setValueAt(Object value, int row, int col) {
        Produto product = lista.get(row);
        switch (col) {
            case 0:
                product.setId((Integer) value); //if column 0 (code)
                break;
            case 1:
                product.setDescricao((String) value);
                break;
            default:
        }
        this.fireTableCellUpdated(row, col);
    }

    public boolean removeProduto(Produto product) {
        int linha = this.lista.indexOf(product);
        boolean result = this.lista.remove(product);
        this.fireTableRowsDeleted(linha,linha);//update JTable
        return result;
    }

    public void adicionaProduto(Produto product) {
        this.lista.add(product);
        //this.fireTableDataChanged();
        this.fireTableRowsInserted(lista.size()-1,lista.size()-1);//update JTable
    }

    public void setListaProdutos(List<Produto> products) {
        this.lista = products;
        this.fireTableDataChanged();
        //this.fireTableRowsInserted(0,contatos.size()-1);//update JTable
    }

    public void limpaTabela() {
        int indice = lista.size()-1;
        if(indice<0)
            indice=0;
        this.lista = new ArrayList<>();
        this.fireTableRowsDeleted(0,indice);//update JTable
    }

    public Produto getProduto(int linha){
        return lista.get(linha);
    }
}
