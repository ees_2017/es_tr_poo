/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ufpr.sistemapedidos.controller;

import org.ufpr.sistemapedidos.dao.impl.ClienteDAOImpl;
import org.ufpr.sistemapedidos.Cliente;
import org.ufpr.sistemapedidos.swing.table.ModeloTabelaCliente;

/**
 *
 * @author noemi
 */
public class ClienteController {
    private ModeloTabelaCliente modeloTabela;
    
    public  void novoCliente(Cliente cliente){
      Cliente cl = new Cliente((int) 0l, cliente.getCpf(), cliente.getNome(), cliente.getSobreNome());

        cliente.setCpf(cl.getCpf());
        cliente.setNome(cl.getNome());
        cliente.setSobreNome(cl.getSobreNome());
          
       ClienteDAOImpl dao = null;
        try {
            dao = new ClienteDAOImpl();
            dao.criar(cliente);
        } catch (Exception ex) {
//            JOptionPane.showMessageDialog(null,"Erro ao atualizar no banco de dados. E="+ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
        }
        modeloTabela.adicionaCliente(cliente);
    }     
    
}
