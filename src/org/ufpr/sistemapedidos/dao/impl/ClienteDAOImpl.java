/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ufpr.sistemapedidos.dao.impl;

import java.io.IOException;
import org.ufpr.sistemapedidos.Cliente;
import org.ufpr.sistemapedidos.util.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.ufpr.sistemapedidos.builders.ClienteBuilder;
import org.ufpr.sistemapedidos.dao.ClienteDAO;

/**
 *
 * @author noemi
 */
public class ClienteDAOImpl implements ClienteDAO {

    private final String insert = "INSERT INTO cliente(cpf,nome,sobrenome) VALUES(?,?,?)";
    private final String select = "SELECT * FROM cliente";
    private final String delete = "DELETE c "
            + "FROM cliente c "
            + " LEFT JOIN pedido p "
            + " ON p.id_cliente = c.id "
            + "WHERE p.id IS NULL "
            + " AND c.id = ?";
    private final String update = "UPDATE cliente SET cpf= ?, nome=?, sobrenome =? WHERE id= ?";
    private final String selectPorId = "SELECT * FROM cliente WHERE id = ?";
    private final String selectPorCpf = "SELECT * FROM cliente WHERE cpf = ?";

    private Connection connection;

    public ClienteDAOImpl() throws SQLException, IOException {
        this.connection = new ConnectionFactory().getConnection();
    }

    @Override
    public Cliente criar(Cliente cliente) throws SQLException {
        try {
            PreparedStatement stmtAdiciona = connection.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
            stmtAdiciona.setString(1, cliente.getCpf());
            stmtAdiciona.setString(2, cliente.getNome());
            stmtAdiciona.setString(3, cliente.getSobreNome());
            stmtAdiciona.execute();

            ResultSet rs = stmtAdiciona.getGeneratedKeys();
            if (rs.next()) {
                cliente.setId(rs.getInt(1));
            }

            return cliente;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Cliente> listarClientes() throws SQLException {
        PreparedStatement stmtLista = this.connection.prepareStatement(this.select);
        ResultSet rs = stmtLista.executeQuery();

        try {
            List<Cliente> clientes = new ArrayList<>();
            while (rs.next()) {
                clientes.add(
                        new ClienteBuilder()
                                .setId(rs.getInt("id"))
                                .setCpf(rs.getString("cpf"))
                                .setNome(rs.getString("nome"))
                                .setSobrenome(rs.getString("sobrenome"))
                                .build()
                );
            }
            return clientes;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                rs.close();
            } catch (SQLException ex) {
                throw new RuntimeException("Falha ao fechar rs.");
            }
            try {
                stmtLista.close();
            } catch (SQLException ex) {
                throw new RuntimeException("Falha ao fechar stmt.");
            }
            try {
                this.connection.close();
            } catch (SQLException ex) {
                throw new RuntimeException("Falha ao fechar conexão.");
            }

        }
    }

    @Override
    public void atualizar(Cliente cliente) throws SQLException {
        PreparedStatement stmtAtualiza = this.connection.prepareStatement(this.update);
        try {
            stmtAtualiza.setString(1, cliente.getCpf());
            stmtAtualiza.setString(2, cliente.getNome());
            stmtAtualiza.setString(3, cliente.getSobreNome());
            stmtAtualiza.setInt(4, cliente.getId());

            stmtAtualiza.executeUpdate();
        } finally {
            try {
                stmtAtualiza.close();
            } catch (SQLException ex) {
                throw new RuntimeException("Falha ao fechar stmt.");
            }
            try {
                this.connection.close();
            } catch (SQLException ex) {
                throw new RuntimeException("Falha ao fechar conexão.");
            }

        }

    }

    @Override
    public int excluir(Cliente cliente) throws SQLException {

        PreparedStatement stmtExcluir = this.connection.prepareStatement(this.delete);
        try {
            stmtExcluir.setInt(1, cliente.getId());
            return stmtExcluir.executeUpdate();
        } catch (SQLException e) {
            if (this.connection != null) {
                try {
                    this.connection.rollback();
                } catch (SQLException exep) {
                    throw new RuntimeException(exep);
                }
            }
        } finally {
            if (stmtExcluir != null) {
                stmtExcluir.close();
            }
        }
        return -1;
    }

    @Override
    public Cliente obter(String cpf) throws SQLException {
        PreparedStatement stmtObter = this.connection.prepareStatement(this.selectPorCpf);;
        ResultSet rs = null;
        try {
            stmtObter.setString(1, cpf);
            rs = stmtObter.executeQuery();
            if (rs.next()) {
                return new ClienteBuilder()
                        .setId(rs.getInt("id"))
                        .setCpf(rs.getString("cpf"))
                        .setNome(rs.getString("nome"))
                        .setSobrenome(rs.getString("sobrenome"))
                        .build();
            } else {
                throw new RuntimeException("CPF inválido=" + cpf);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                throw new RuntimeException("Falha ao fechar rs.");
            }
            try {
                if (stmtObter != null) {
                    stmtObter.close();
                }
            } catch (SQLException ex) {
                throw new RuntimeException("Falha ao fechar stmt.");
            }
            try {
                if (this.connection != null) {
                    this.connection.close();
                }
            } catch (SQLException ex) {
                throw new RuntimeException("Falha ao fechar conexão.");
            }

        }
    }

    @Override
    public Cliente obter(int id) throws SQLException {
        PreparedStatement stmtObter = this.connection.prepareStatement(this.selectPorId);;
        ResultSet rs = null;
        try {
            stmtObter.setInt(1, id);
            rs = stmtObter.executeQuery();
            if (rs.next()) {
                return new ClienteBuilder()
                        .setId(rs.getInt("id"))
                        .setCpf(rs.getString("cpf"))
                        .setNome(rs.getString("nome"))
                        .setSobrenome(rs.getString("sobrenome"))
                        .build();
            } else {
                throw new RuntimeException("Id inválido=" + id);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                rs.close();
            } catch (SQLException ex) {
                throw new RuntimeException("Falha ao fechar rs.");
            }
            try {
                stmtObter.close();
            } catch (SQLException ex) {
                throw new RuntimeException("Falha ao fechar stmt.");
            }
            try {
                this.connection.close();
            } catch (SQLException ex) {
                throw new RuntimeException("Falha ao fechar conexão.");
            }

        }
    }
}
