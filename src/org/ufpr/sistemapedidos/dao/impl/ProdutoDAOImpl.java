/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ufpr.sistemapedidos.dao.impl;

import com.mysql.jdbc.Statement;
import java.io.IOException;
import org.ufpr.sistemapedidos.util.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.ufpr.sistemapedidos.Produto;
import org.ufpr.sistemapedidos.builders.ProdutoBuilder;
import org.ufpr.sistemapedidos.dao.ProdutoDAO;

/**
 *
 * @author noemi
 */
public class ProdutoDAOImpl implements ProdutoDAO {

    private String insert = "INSERT INTO produto(descricao) VALUES(?)";
    private String select = "SELECT * FROM produto";
    private String delete = "DELETE FROM produto WHERE id=?;";
    private String update = "UPDATE produto SET descricao= ? WHERE id=?";
    private String selectPorId = "SELECT * FROM produto WHERE id=?";

    private Connection connection;

    public ProdutoDAOImpl() throws SQLException, IOException {
        this.connection = new ConnectionFactory().getConnection();
    }

    @Override
    public void criar(Produto produto) throws SQLException {
        try {
            PreparedStatement stmtAdiciona = this.connection.prepareStatement(this.insert, Statement.RETURN_GENERATED_KEYS);
            stmtAdiciona.setString(1, produto.getDescricao());
            stmtAdiciona.execute();

            ResultSet rs = stmtAdiciona.getGeneratedKeys();
            rs.next();
            produto.setId(rs.getInt(1));

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Produto> listar() throws SQLException {
        PreparedStatement stmtLista = null;
        ResultSet rs = null;

        try {
            //con = ConnectionFactory.getConnection();
            stmtLista = this.connection.prepareStatement(select);
            rs = stmtLista.executeQuery();

            List<Produto> produtos = new ArrayList<>();
            while (rs.next()) {

                Produto produto = new ProdutoBuilder()
                        .setId(rs.getInt("id"))
                        .setDescricao(rs.getString("descricao"))
                        .build();
                produtos.add(produto);
            }
            return produtos;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                rs.close();
            } catch (SQLException ex) {
                throw new RuntimeException("Falha ao fechar rs.");
            }
            try {
                stmtLista.close();
            } catch (SQLException ex) {
                throw new RuntimeException("Falha ao fechar stmt.");
            }
            try {
                this.connection.close();
            } catch (SQLException ex) {
                throw new RuntimeException("Falha ao fechar conexão.");
            }

        }
    }

    @Override
    public void atualizar(Produto produto) throws SQLException {
        PreparedStatement stmtAtualiza = null;
        try {
            //con = ConnectionFactory.getConnection();
            stmtAtualiza = this.connection.prepareStatement(update);
            stmtAtualiza.setString(1, produto.getDescricao());
            stmtAtualiza.setLong(2, produto.getId());

            stmtAtualiza.executeUpdate();
        } finally {
            try {
                stmtAtualiza.close();
            } catch (SQLException ex) {
                throw new RuntimeException("Falha ao fechar stmt.");
            }
            try {
                this.connection.close();
            } catch (SQLException ex) {
                throw new RuntimeException("Falha ao fechar conexão.");
            }

        }

    }

    @Override
    public void excluir(Produto produto) throws SQLException {

        PreparedStatement stmtExcluir = this.connection.prepareStatement(delete);
        try {
            stmtExcluir.setLong(1, produto.getId());
            stmtExcluir.executeUpdate();
        } finally {
            stmtExcluir.close();

        }
    }

    @Override
    public Produto obter(int id) throws SQLException {
        PreparedStatement stmtObter = null;
        ResultSet rs = null;
        ;
        try {
            stmtObter = this.connection.prepareStatement(selectPorId);
            stmtObter.setLong(1, id);
            rs = stmtObter.executeQuery();
            Produto produto;
            if (rs.next()) {
                produto = new ProdutoBuilder()
                        .setDescricao(rs.getString("descricao"))
                        .setId(rs.getInt("id"))
                        .build();
                return produto;
            } else {
                throw new RuntimeException("Id inválido=" + id);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                throw new RuntimeException("Falha ao fechar rs.");
            }
            try {
                if (stmtObter != null) {
                    stmtObter.close();
                }
            } catch (SQLException ex) {
                throw new RuntimeException("Falha ao fechar stmt.");
            }
            try {
                if (this.connection != null) {
                    this.connection.close();
                }
            } catch (SQLException ex) {
                throw new RuntimeException("Falha ao fechar conexão.");
            }
        }
    }

}
