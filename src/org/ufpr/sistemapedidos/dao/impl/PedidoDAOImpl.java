/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ufpr.sistemapedidos.dao.impl;

import com.sun.javafx.scene.control.skin.VirtualFlow;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ufpr.sistemapedidos.Cliente;
import org.ufpr.sistemapedidos.ItemDoPedido;
import org.ufpr.sistemapedidos.Pedido;
import org.ufpr.sistemapedidos.builders.ClienteBuilder;
import org.ufpr.sistemapedidos.builders.PedidoBuilder;
import org.ufpr.sistemapedidos.dao.PedidoDAO;
import org.ufpr.sistemapedidos.util.ConnectionFactory;

/**
 *
 * @author luan
 */
public class PedidoDAOImpl implements PedidoDAO {

    private final String insert = "INSERT INTO pedido(data,id_cliente) VALUES(?,?)";

    private final String select = "SELECT * from pedido";
    private final String selectPorId = "SELECT * FROM pedido WHERE id=?;";
    private final String selectPorCliente = "SELECT p.* FROM pedido p LEFT JOIN cliente c ON p.id_cliente = c.id WHERE cpf = ?;";

    private final String delete = "DELETE FROM pedido WHERE id=?;";
    private final String update = "UPDATE pedido SET data=?, id_cliente=? WHERE id= ?";

    private Connection connection;

    public PedidoDAOImpl() throws SQLException, IOException {
        this.connection = new ConnectionFactory().getConnection();
    }

    @Override
    public List<Pedido> listarPedidos() throws SQLException, IOException {
        Connection con = null;
        PreparedStatement stmtLista = this.connection.prepareStatement(this.select);

        ResultSet rs = stmtLista.executeQuery();

        List<Pedido> pedidos = new ArrayList<>();
        while (rs.next()) {
            pedidos.add(
                    new PedidoBuilder()
                            .setId(rs.getInt("id"))
                            .setData(rs.getString("data"))
                            .setCliente(new ClienteDAOImpl().obter(rs.getInt("id_cliente")))
                            .build()
            );
        }
        return pedidos;
    }

    @Override
    public List<Pedido> listarPedidos(Cliente cliente) throws SQLException, IOException {
        Connection con = null;
        PreparedStatement stmtLista = this.connection.prepareStatement(this.selectPorCliente);
        stmtLista.setString(1,cliente.getCpf());

        ResultSet rs = stmtLista.executeQuery();

        List<Pedido> pedidos = new ArrayList<>();
        while (rs.next()) {
            pedidos.add(
                    new PedidoBuilder()
                            .setId(rs.getInt("id"))
                            .setData(rs.getString("data"))
                            .setCliente(new ClienteDAOImpl().obter(rs.getInt("id_cliente")))
                            .build()
            );
        }
        return pedidos;
    }

    @Override
    public Pedido criar(Pedido pedido) {
        try {
            PreparedStatement stmtAdiciona = this.connection.prepareStatement(this.insert, Statement.RETURN_GENERATED_KEYS);
            stmtAdiciona.setString(1, new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
            stmtAdiciona.setInt(2, pedido.getCliente().getId());
            stmtAdiciona.execute();

            ResultSet rs = stmtAdiciona.getGeneratedKeys();
            Pedido pedidoCriado = null;
            if (rs.next()) {
                pedidoCriado = this.obter(rs.getInt(1));
                List<ItemDoPedido> itensDoPedidoCriado;
                itensDoPedidoCriado = new ArrayList<>();
                for (ItemDoPedido item : pedido.getItens()) {
                    item.setPedido(pedidoCriado);
                    itensDoPedidoCriado.add(new ItemDoPedidoDAOImpl().criar(item));
                }
            }
            return pedidoCriado;
        } catch (IOException | SQLException ex) {
            Logger.getLogger(PedidoDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException("Falha ao criar pedido.");
        }
    }

    @Override
    public void atualizar(Pedido pedido) throws SQLException, IOException {

        PreparedStatement stmtAtualiza = this.connection.prepareStatement(this.update);
        try {
            stmtAtualiza.setString(1, pedido.getData());
            stmtAtualiza.setLong(2, pedido.getCliente().getId());
            stmtAtualiza.setInt(3, pedido.getId());

            stmtAtualiza.executeUpdate();
        } finally {
            try {
                stmtAtualiza.close();
            } catch (SQLException ex) {
                throw new RuntimeException("Falha ao fechar stmt.");
            }
            try {
                this.connection.close();
            } catch (SQLException ex) {
                throw new RuntimeException("Falha ao fechar conexão.");
            }

        }

    }

    @Override
    public void excluir(Pedido pedido){
        PreparedStatement stmtExcluir = null;
        try {
            stmtExcluir = this.connection.prepareStatement(this.delete);
            stmtExcluir.setInt(1, pedido.getId());
            stmtExcluir.executeUpdate();
        } catch (SQLException ex) {
            throw new RuntimeException("Falha ao excluir. \n" + ex.getMessage());
        } finally {
            try {
                if(stmtExcluir != null) stmtExcluir.close();
            } catch (SQLException ex) {
                throw new RuntimeException("Falha ao fechar stmt. \n" + ex.getMessage());
            }
        }
    }

    @Override
    public Pedido obter(int id) throws SQLException, IOException {
        ResultSet rs = null;
        PreparedStatement stmtObter = null;
        try {
            stmtObter = this.connection.prepareStatement(this.selectPorId);
            //con = ConnectionFactory.getConnection();
            stmtObter.setInt(1, id);
            rs = stmtObter.executeQuery();

            if (rs.next()) {
                return new PedidoBuilder()
                        .setId(rs.getInt("id"))
                        .setData(rs.getString("data"))
                        .setCliente(new ClienteDAOImpl().obter(rs.getInt("id_cliente")))
                        .build();
            } else {
                throw new RuntimeException("Id inválido=" + id);
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                throw new RuntimeException("Falha ao fechar rs.");
            }
            try {
                if (stmtObter != null) {
                    stmtObter.close();
                }
            } catch (SQLException ex) {
                throw new RuntimeException("Falha ao fechar stmt.");
            }
            try {
                if (this.connection != null) {
                    this.connection.close();
                }
            } catch (SQLException ex) {
                throw new RuntimeException("Falha ao fechar conexão.");
            }

        }
    }

}
