/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ufpr.sistemapedidos.dao.impl;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ufpr.sistemapedidos.ItemDoPedido;
import org.ufpr.sistemapedidos.Pedido;
import org.ufpr.sistemapedidos.Produto;
import org.ufpr.sistemapedidos.builders.ItemDoPedidoBuilder;
import org.ufpr.sistemapedidos.dao.ItemDoPedidoDAO;
import org.ufpr.sistemapedidos.util.ConnectionFactory;

public class ItemDoPedidoDAOImpl implements ItemDoPedidoDAO {

    private Connection connection;
    private PreparedStatement stmt;

    private final String listar = "SELECT * "
            + "FROM item_do_pedido AS i "
            + " INNER JOIN  pedido AS p "
            + "  ON p.id = i.id_pedido "
            + "WHERE p.id=?";
    private final String obter = "SELECT * FROM item_do_pedido WHERE id=?";
    private final String update = "UPDATE item_do_pedido SET id_pedido=?, id_produto=?, qtdade=? WHERE id=?";
    private final String delete = "DELETE FROM item_do_pedido WHERE id=?";
    private final String insert = "INSERT INTO item_do_pedido(id_pedido, id_produto, qtdade) VALUES(?, ?, ?)";

    public ItemDoPedidoDAOImpl() throws IOException {
        this.connection = new ConnectionFactory().getConnection();
    }

    @Override
    public List<ItemDoPedido> listar(Pedido pedido) throws SQLException {
        try {
            this.stmt = this.connection.prepareStatement(listar);
            this.stmt.setInt(1, pedido.getId());
            ResultSet rs = this.stmt.executeQuery();
            List<ItemDoPedido> itensDoPedido = new ArrayList<>();
            while (rs.next()) {
                Produto produto = new ProdutoDAOImpl().obter(rs.getInt("id_produto"));
                itensDoPedido.add(
                        new ItemDoPedidoBuilder()
                                .setProduct(produto)
                                .setPedido(pedido)
                                .setQuantity(rs.getInt("qtdade"))
                                .build()
                );

            }
            this.connection.close();
            return itensDoPedido;
        } catch (IOException ex) {
            Logger.getLogger(ItemDoPedidoDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex.getMessage());
        }
    }

    @Override
    public ItemDoPedido obter(int id) throws SQLException {
        this.stmt = this.connection.prepareStatement(obter);
        this.stmt.setLong(1, id);
        ResultSet rs = this.stmt.executeQuery();
        if (rs.next()) {
            try {
                return new ItemDoPedidoBuilder()
                        .setProduct(new ProdutoDAOImpl().obter(rs.getInt("id_produto")))
                        .build();
            } catch (IOException ex) {
                Logger.getLogger(ItemDoPedidoDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            throw new RuntimeException("Id inválido=" + id);
        }
        return null;
    }

    @Override
    public int atualizar(ItemDoPedido itemDoPedido) throws SQLException {
        this.stmt = this.connection.prepareStatement(this.update);
        this.stmt.setInt(1, itemDoPedido.getPedido().getId());
        this.stmt.setLong(2, itemDoPedido.getProduto().getId());
        this.stmt.setInt(3, itemDoPedido.getQuantidade());
        this.stmt.setInt(4, itemDoPedido.getId());
        return this.stmt.executeUpdate();
    }

    @Override
    public int excluir(ItemDoPedido itemDopedido) throws SQLException {
        this.stmt = this.connection.prepareStatement(this.delete);
        return this.stmt.executeUpdate();
    }

    @Override
    public ItemDoPedido criar(ItemDoPedido itemDoPedido) {
        try {
            this.stmt = this.connection.prepareStatement(this.insert, Statement.RETURN_GENERATED_KEYS);
            this.stmt.setInt(1, itemDoPedido.getPedido().getId());
            this.stmt.setLong(2, itemDoPedido.getProduto().getId());
            this.stmt.setInt(3, itemDoPedido.getQuantidade());
            this.stmt.executeUpdate();

            ResultSet rs = this.stmt.getGeneratedKeys();
            if (rs.next()) {
                itemDoPedido.setId(rs.getInt(1));
            }
            return itemDoPedido;
        } catch (SQLException ex) {
            Logger.getLogger(ItemDoPedidoDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
//            throw new RuntimeException(ex.getMessage());
        }
        return null;

    }

}
