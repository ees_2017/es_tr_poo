/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ufpr.sistemapedidos.dao;

import java.sql.SQLException;
import java.util.List;
import org.ufpr.sistemapedidos.Cliente;

/**
 *
 * @author lsantos
 */
public interface ClienteDAO {

    public Cliente criar(Cliente cliente) throws SQLException;

    public List<Cliente> listarClientes() throws SQLException;

    public Cliente obter(String cpf) throws SQLException;
    
    public Cliente obter(int id) throws SQLException;

    public void atualizar(Cliente cliente) throws SQLException;

    public int excluir(Cliente cliente) throws SQLException;
}
