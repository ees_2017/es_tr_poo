/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ufpr.sistemapedidos.dao;

import java.sql.SQLException;
import java.util.List;
import org.ufpr.sistemapedidos.ItemDoPedido;
import org.ufpr.sistemapedidos.Pedido;

/**
 *
 * @author luan
 */
public interface ItemDoPedidoDAO {

    public List<ItemDoPedido> listar(Pedido pedido) throws SQLException;
    
    public ItemDoPedido criar(ItemDoPedido itemDoPedido) throws SQLException;

    public ItemDoPedido obter(int id) throws SQLException;

    public int atualizar(ItemDoPedido itemDoPedido) throws SQLException;

    public int excluir(ItemDoPedido itemDoPedido) throws SQLException;
}
