/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ufpr.sistemapedidos.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import org.ufpr.sistemapedidos.Cliente;
import org.ufpr.sistemapedidos.Pedido;

/**
 *
 * @author luan
 */
public interface PedidoDAO {

    public List<Pedido> listarPedidos() throws SQLException, IOException;
    
    public List<Pedido> listarPedidos(Cliente cliente) throws SQLException, IOException;

    public Pedido obter(int rollNo) throws SQLException, IOException;

    public void atualizar(Pedido pedido) throws SQLException, IOException;

    public void excluir(Pedido pedido) throws SQLException, IOException;
    
    public Pedido criar(Pedido pedido) throws SQLException, IOException;
}
