/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ufpr.sistemapedidos.dao;

import java.sql.SQLException;
import java.util.List;
import org.ufpr.sistemapedidos.Produto;

/**
 *
 * @author lsantos
 */
public interface ProdutoDAO {

    public List<Produto> listar() throws SQLException;
    
    public void criar(Produto produto) throws SQLException;

    public Produto obter(int id) throws SQLException;

    public void atualizar(Produto cliente) throws SQLException;

    public void excluir(Produto cliente) throws SQLException;
}
