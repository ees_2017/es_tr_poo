/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ufpr.sistemapedidos;

import java.util.Objects;

public class ItemDoPedido {

    private int id;
    private int quantidade;
    private Produto produto;
    private Pedido pedido;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public ItemDoPedido(int quantidade, Produto produto) {
        this.quantidade = quantidade;
        this.produto = produto;
    }

    public ItemDoPedido() {
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        ItemDoPedido rhs = (ItemDoPedido) obj;

        return this.quantidade == rhs.quantidade && this.produto.equals(rhs.produto);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 31 * hash + this.quantidade;
        hash = 31 * hash + Objects.hashCode(this.produto);
        return hash;
    }

    @Override
    public String toString() {
        return this.produto.toString() + "(" + this.quantidade + "x)[" + this.pedido.getId() + "]";
    }

}
