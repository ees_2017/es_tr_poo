/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ufpr.sistemapedidos.builders;

import org.ufpr.sistemapedidos.ItemDoPedido;
import org.ufpr.sistemapedidos.Pedido;
import org.ufpr.sistemapedidos.Produto;

/**
 *
 * @author luan
 */
public class ItemDoPedidoBuilder {
    
    public ItemDoPedido item;

    public ItemDoPedidoBuilder() {
        this.item = new ItemDoPedido();
    }
    
    public ItemDoPedido build(){
        return this.item;
    }
    
    public ItemDoPedidoBuilder setQuantity(int quantity){
        this.item.setQuantidade(quantity);
        return this;
    }
    
    public ItemDoPedidoBuilder setProduct(Produto product){
        this.item.setProduto(product);
        return this;
    }
    
    public ItemDoPedidoBuilder setPedido(Pedido pedido) {
        this.item.setPedido(pedido);
        return this;
    }
}
