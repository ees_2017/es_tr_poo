/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ufpr.sistemapedidos.builders;

import org.ufpr.sistemapedidos.Produto;

/**
 *
 * @author luan
 */
public class ProdutoBuilder {
    
    public Produto product;

    public ProdutoBuilder() {
        this.product = new Produto();
    }
    
    public Produto build(){
        return this.product;
    }
    
    public ProdutoBuilder setId(int id){
        this.product.setId(id);
        return this;
    }
    
    public ProdutoBuilder setDescricao(String description){
        this.product.setDescricao(description);
        return this;
    }
}
