/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ufpr.sistemapedidos.builders;

import java.util.ArrayList;
import java.util.List;
import org.ufpr.sistemapedidos.Cliente;
import org.ufpr.sistemapedidos.ItemDoPedido;
import org.ufpr.sistemapedidos.Pedido;

/**
 *
 * @author luan
 */
public class PedidoBuilder {

    public Pedido pedido;
    public Cliente client;
    public List<ItemDoPedido> items;

    public PedidoBuilder() {
        this.pedido = new Pedido();
        this.items = new ArrayList<>();
    }

    public PedidoBuilder setId(Integer id) {
        this.pedido.setId(id);
        return this;
    }

    public PedidoBuilder setCliente(Cliente client) {
        this.client = client;
        return this;
    }

    public PedidoBuilder setData(String date) {
        this.pedido.setData(date);
        return this;
    }

    public PedidoBuilder addItem(ItemDoPedido item) {
        if(this.items.contains(item)){
        }
        this.items.add(item);
        return this;
    }
    
    public PedidoBuilder addItens(List<ItemDoPedido> itens) {
        itens.forEach(this::addItem);
        return this;
    }

    public Pedido build() {
        this.pedido.setItens(this.items);
        this.pedido.setCliente(this.client);
        return this.pedido;
    }

}
