/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ufpr.sistemapedidos.builders;

import org.ufpr.sistemapedidos.Cliente;

/**
 *
 * @author luan
 */
public class ClienteBuilder {

    public Cliente cliente;
    
    public ClienteBuilder() {
        this.cliente = new Cliente();
    }
    
    public Cliente build(){
        return this.cliente;
    }
    
    public ClienteBuilder setId(int id) {
        this.cliente.setId(id);
        return this;
    }
    
    public ClienteBuilder setNome(String name){
        this.cliente.setNome(name);
        return this;
    }
    
    public ClienteBuilder setCpf(String cpf){
        this.cliente.setCpf(cpf);
        return this;
    }
    
    public ClienteBuilder setSobrenome(String surname){
        this.cliente.setSobreNome(surname);
        return this;
    }

}
