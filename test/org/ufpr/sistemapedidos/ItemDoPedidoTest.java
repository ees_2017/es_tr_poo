/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ufpr.sistemapedidos;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author luan
 */
public class ItemDoPedidoTest {

    private Produto product;
    private String productDescription = "Papel A4";
    private int productID = 1;
    private ItemDoPedido item;
    private int quantity = 2;
    
    public ItemDoPedidoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        this.product = new Produto(this.productID, this.productDescription);
        this.item = new ItemDoPedido(this.quantity, this.product);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getQuantidade method, of class ItemDoPedido.
     */
    @Test
    public void testGetQuantidade() {
        assertEquals(this.quantity, item.getQuantidade());
    }

    /**
     * Test of setQuantidade method, of class ItemDoPedido.
     */
    @Test
    public void testSetQuantidade() {
        int expectedQuantity = 3;
        item.setQuantidade(expectedQuantity);
        assertEquals(expectedQuantity, item.getQuantidade());
    }

    /**
     * Test of getProduto method, of class ItemDoPedido.
     */
    @Test
    public void testGetProduto() {
        Produto expectedProduct = new Produto(this.productID, this.productDescription);
        assertEquals( true, this.product.equals(expectedProduct));
    }

    /**
     * Test of setProduto method, of class ItemDoPedido.
     */
    @Test
    public void testSetProduto() {
        int anotherID = this.productID + 1;
        String anotherDescription = productDescription + " another";
        Produto anotherProduct = new Produto(anotherID, anotherDescription);
        item.setProduto(anotherProduct);
        assertEquals(true, anotherProduct.equals(item.getProduto()));
    }
    
}
