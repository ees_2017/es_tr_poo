/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ufpr.sistemapedidos.builders;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.ufpr.sistemapedidos.Cliente;
import org.ufpr.sistemapedidos.ItemDoPedido;
import org.ufpr.sistemapedidos.Pedido;

/**
 *
 * @author luan
 */
public class PedidoBuilderTest {
    
    public PedidoBuilderTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setId method, of class PedidoBuilder.
     */
    @Test
    public void testSetId() {
        int id = 0;
        Pedido instance = new PedidoBuilder().setId(id).build();
        assertEquals(instance.getId(), id);
    }

    /**
     * Test of setCliente method, of class PedidoBuilder.
     */
    @Test
    public void testSetCliente() {
        System.out.println("setCliente");
        Cliente client = null;
        PedidoBuilder instance = new PedidoBuilder();
        PedidoBuilder expResult = null;
        PedidoBuilder result = instance.setCliente(client);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setData method, of class PedidoBuilder.
     */
    @Test
    public void testSetData() {
        System.out.println("setData");
        String date = "";
        PedidoBuilder instance = new PedidoBuilder();
        PedidoBuilder expResult = null;
        PedidoBuilder result = instance.setData(date);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addItem method, of class PedidoBuilder.
     */
    @Test
    public void testAddItem() {
        System.out.println("addItem");
        ItemDoPedido item = null;
        PedidoBuilder instance = new PedidoBuilder();
        PedidoBuilder expResult = null;
        PedidoBuilder result = instance.addItem(item);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addItens method, of class PedidoBuilder.
     */
    @Test
    public void testAddItens() {
        System.out.println("addItens");
        List<ItemDoPedido> itens = null;
        PedidoBuilder instance = new PedidoBuilder();
        PedidoBuilder expResult = null;
        PedidoBuilder result = instance.addItens(itens);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of build method, of class PedidoBuilder.
     */
    @Test
    public void testBuild() {
        System.out.println("build");
        Pedido instance = new PedidoBuilder().build();
        Pedido expResult = new Pedido();
        expResult.setId(0);
        System.out.println(expResult);
        System.out.println(instance);
//        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
