/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ufpr.sistemapedidos;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.ufpr.sistemapedidos.builders.ClienteBuilder;

/**
 *
 * @author luan
 */
public class ClienteTest {
    
    Cliente cliente;
    int id = 1;
    String cpf = "000.000.000-00";
    String name = "Name";
    String surname = "Surname";

    public ClienteTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        cliente = new ClienteBuilder().setId(id).setCpf(cpf).setNome(name).setSobrenome(surname).build();
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getId method, of class Cliente.
     */
    @Test
    public void testGetId() {
        assertEquals(this.id, cliente.getId());
    }

    /**
     * Test of setId method, of class Cliente.
     */
    @Test
    public void testSetId() {
        int expectedId = 3;
        cliente.setId(expectedId);
        assertEquals(expectedId, cliente.getId());
    }

    /**
     * Test of getCpf method, of class Cliente.
     */
    @Test
    public void testGetCpf() {
        assertEquals(this.cpf, cliente.getCpf());
    }

    /**
     * Test of setCpf method, of class Cliente.
     */
    @Test
    public void testSetCpf() {
        String expectedCpf = "000.000.000-01";
        cliente.setCpf(expectedCpf);
        assertEquals(expectedCpf, cliente.getCpf());
    }

    /**
     * Test of getNome method, of class Cliente.
     */
    @Test
    public void testGetNome() {
        assertEquals(this.name, cliente.getNome());
    }

    /**
     * Test of setNome method, of class Cliente.
     */
    @Test
    public void testSetNome() {
        String expectedNome = "New Name";
        cliente.setNome(expectedNome);
        assertEquals(expectedNome, cliente.getNome());
    }

    /**
     * Test of getSobreNome method, of class Cliente.
     */
    @Test
    public void testGetSobreNome() {
        assertEquals(this.surname, cliente.getSobreNome());
    }

    /**
     * Test of setSobreNome method, of class Cliente.
     */
    @Test
    public void testSetSobreNome() {
        String expectedSurname = "New Surname";
        cliente.setSobreNome(expectedSurname);
        assertEquals(expectedSurname, cliente.getSobreNome());
        
    }

}
