/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ufpr.sistemapedidos;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.ufpr.sistemapedidos.builders.ClienteBuilder;
import org.ufpr.sistemapedidos.builders.ItemDoPedidoBuilder;
import org.ufpr.sistemapedidos.builders.PedidoBuilder;
import org.ufpr.sistemapedidos.builders.ProdutoBuilder;

/**
 *
 * @author luan
 */
public class PedidoTest {

    private Pedido pedido;
    private int id = 0;
    private String date;
    private Cliente client;
    private Produto produto1;
    private Produto produto2;
    private Produto produto3;

    public PedidoTest() {
        this.id = 0;
        this.date = "2017-08-31";
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        this.client = new ClienteBuilder().setCpf("000.000.000-00").setNome("Name").setSobrenome("Surname").build();

        produto1 = new ProdutoBuilder().setId(0).setDescricao("Papel A2").build();
        produto2 = new ProdutoBuilder().setId(0).setDescricao("Papel A3").build();
        produto3 = new ProdutoBuilder().setId(0).setDescricao("Papel A4").build();

        pedido = new PedidoBuilder()
                .setCliente(this.client)
                .setId(this.id)
                .setData(this.date)
                .addItem(new ItemDoPedidoBuilder().setQuantity(2).setProduct(this.produto1).build())
                .addItem(new ItemDoPedidoBuilder().setQuantity(2).setProduct(this.produto2).build())
                .addItem(new ItemDoPedidoBuilder().setQuantity(2).setProduct(this.produto3).build())
                .build();
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getId method, of class Pedido.
     */
    @Test
    public void testGetId() {
        assertEquals(0, this.pedido.getId());
    }

    /**
     * Test of setId method, of class Pedido.
     */
    @Test
    public void testSetId() {
        int expectedId = 3;
        this.pedido.setId(expectedId);
        assertEquals(expectedId, this.pedido.getId());
    }

    /**
     * Test of getData method, of class Pedido.
     */
    @Test
    public void testGetData() {
        assertEquals(this.date, this.pedido.getData());
    }

    /**
     * Test of setData method, of class Pedido.
     */
    @Test
    public void testSetData() {
        String expectedDate = "2017-09-30";
        this.pedido.setData(expectedDate);
        assertEquals(expectedDate, this.pedido.getData());
    }

    /**
     * Test of getCliente method, of class Pedido.
     */
    @Test
    public void testGetCliente() {
        assertEquals(true, this.pedido.getCliente().equals(this.client));
    }

    /**
     * Test of setCliente method, of class Pedido.
     */
    @Test
    public void testSetCliente() {
        Cliente client2 = new ClienteBuilder()
                .setId(this.client.getId() + 1)
                .setCpf(this.client.getCpf())
                .setNome(this.client.getNome())
                .setSobrenome(this.client.getSobreNome())
                .build();
        this.pedido.setCliente(client2);
        assertEquals(true, this.pedido.getCliente().equals(client2));
    }

    /**
     * Test of getItens method, of class Pedido.
     */
    @Test
    public void testGetItens() {
        ItemDoPedido item1 = new ItemDoPedidoBuilder().setQuantity(0).setProduct(this.produto1).build();
        ItemDoPedido item2 = new ItemDoPedidoBuilder().setQuantity(0).setProduct(this.produto2).build();
        ItemDoPedido item3 = new ItemDoPedidoBuilder().setQuantity(0).setProduct(this.produto3).build();
        
        List<ItemDoPedido> expectedItens = new ArrayList<>();
        expectedItens.add(item1);
        expectedItens.add(item2);
        expectedItens.add(item3);
        
        List<ItemDoPedido> comparedItens = new ArrayList<>();
        
        for (ItemDoPedido itemDoPedido : this.pedido.getItens()) {
            for(ItemDoPedido expectedItemDoPedido: expectedItens){
                if (expectedItemDoPedido.equals(itemDoPedido)){
                    comparedItens.add(itemDoPedido);
                }
            }
        }
    }

    /**
     * Test of setItens method, of class Pedido.
     */
    @Test
    public void testSetItens() {
        System.out.println("setItens");
        List<ItemDoPedido> itens = null;
        Pedido instance = null;
        instance.setItens(itens);
        fail("The test case is a prototype.");
    }

}
