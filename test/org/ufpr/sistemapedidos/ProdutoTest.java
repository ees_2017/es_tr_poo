/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ufpr.sistemapedidos;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author luan
 */
public class ProdutoTest {
    
    Produto product;
    int id = 1;
    String description = "Papel A4";
    
    public ProdutoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        product = new Produto(id, description);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getId method, of class Produto.
     */
    @Test
    public void testGetId() {
        assertEquals(this.id, product.getId());
    }

    /**
     * Test of setId method, of class Produto.
     */
    @Test
    public void testSetId() {
        int expectedId = 0;
        product.setId(expectedId);
        assertEquals(expectedId, product.getId());
    }

    /**
     * Test of getDescricao method, of class Produto.
     */
    @Test
    public void testGetDescricao() {
        assertEquals(this.description, product.getDescricao());
    }

    /**
     * Test of setDescricao method, of class Produto.
     */
    @Test
    public void testSetDescricao() {
        String expectedDescription = "Paper A4";
        product.setDescricao(expectedDescription);
        assertEquals(expectedDescription, product.getDescricao());
    }
    
}
